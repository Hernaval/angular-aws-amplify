import { Component } from '@angular/core';
import {TranslocoService} from "@ngneat/transloco";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private readonly translocoService: TranslocoService) {

  }

  title = 'frontend';
  languages = [
    {
      key: 'fr',
      label: "French"
    },
    {
      key: 'en',
      label: "Anglais"
    }
  ]

  change(v: string) {
    this.translocoService.setActiveLang(v)
  }
}
